package com.tiara.pocketdairy.rest;

import com.tiara.pocketdairy.model.UserPostRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by vinod on 11/2/17.
 */


public interface ApiInterface {

    @GET("login.php")
    Call<JSONArray> getLoginResponse();

    @POST("register.php")
    Call<JSONObject> registerUser(@Body UserPostRequest userPostRequest);

//    @GET("movie/{id}")
//    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}

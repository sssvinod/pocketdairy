package com.tiara.pocketdairy.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vinod on 11/2/17.
 */

public class ApiClient {

    public static final String BASE_URL = "https://petadopt360.inv8.com/edairy/apis/";
    private static Retrofit retrofit = null;

//    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

//    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())

                    .build();
        }
        return retrofit;
    }
}

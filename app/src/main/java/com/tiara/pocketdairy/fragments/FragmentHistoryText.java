package com.tiara.pocketdairy.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.pocketdairy.R;

/**
 * Created by vinod on 12/27/17.
 */

public class FragmentHistoryText extends Fragment{

    View rootView;

    public FragmentHistoryText(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_historytext, container, false);

        initValues();

        return rootView;
    }

    private void initValues() {

    }


}

package com.tiara.pocketdairy.utils

import android.content.Context
import android.net.ConnectivityManager
import android.text.TextUtils
import java.util.regex.Pattern

/**
 * Created by vinod on 11/2/17.
 */

public class Utils {

    /**
     * method to check emial vaild pattern

     * @param email
     * *
     * @return
     */
    fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    /**
     * method to check valid password pattern

     * @param password
     * *
     * @return
     */
    fun isValidPassword(password: String): Boolean {

        val letter = Pattern.compile("[a-zA-z]")
        val digit = Pattern.compile("[0-9]")

        return digit.matcher(password).find() && letter.matcher(password).find()
    }

    fun isNetworkAvailable(mContext: Context): Boolean {

        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            return true
        }
        return false
    }

}
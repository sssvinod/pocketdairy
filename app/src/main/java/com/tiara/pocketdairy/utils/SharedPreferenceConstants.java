package com.tiara.pocketdairy.utils;

/**
 * Created by vinay on 3/9/16.
 * <p/>
 * Class contains all the sharedPreference Constants.
 */
public class SharedPreferenceConstants {

    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String IS_FIRST_LAUNCH = "is_first_launch";
    public static final String VERSION_NUMBER = "version_number";
    public static final String AUTH_KEY = "authkey";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String PHONE_OTP = "phone_otp";
    public static final String APP_VERSION = "app_version";
    public static final String REG_AS = "reg_as";
    public static final String LOGIN_COUNT = "login_count";
    public static final String REG_ON = "registered_on";
    public static final String NAME = "name";
    public static final String IS_AUTO_DOWNLOAD = "is_auto_download";
    public static final String IS_EMAIL_VERIFIED = "is_email_verified";
    public static final String IS_PHONE_VERIFIED = "is_phone_verified";
    public static final String _ID = "_id";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_NETWORK_TYPE = "social_network_type";
    public static final String VERSION = "__v";
    public static final String FREE_SUBSCRIPTION_COUNT = "free_subscription_count";
    public static final String IS_FCM_TOKEN_SEND_TO_SERVER = "is_token_sent";
    public static final String DEVICE_ID = "device_id";

}

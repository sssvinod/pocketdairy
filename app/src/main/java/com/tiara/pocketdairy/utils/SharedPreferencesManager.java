package com.tiara.pocketdairy.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by vinay on 3/9/16.
 * <p/>
 * Class which manages the shared preferences activities like storing and retrving the data from shared preference.
 */
public class SharedPreferencesManager {


    /**
     * Stores the data to the specified key value.
     *
     * @param context
     * @param key
     * @param value
     */
    public static void set(final Context context, final String key, final String value) {
        SharedPreferences preference = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Set boolean value for key
     *
     * @param c
     * @param key
     * @param value
     */
    public static void set(final Context c, final String key, final boolean value) {
        final SharedPreferences preferences = c.getSharedPreferences(key, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Set int value for key
     *
     * @param c
     * @param key
     * @param value
     */
    public static void set(final Context c, final String key, final double value) {
        final SharedPreferences preferences = c.getSharedPreferences(key, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, Double.doubleToLongBits(value));
        editor.commit();
    }

    public static void set(final Context c, final String key, final long value) {
        final SharedPreferences preferences = c.getSharedPreferences(key, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * Get String value for a particular key
     *
     * @param context
     * @param key
     * @return String value that was stored earlier, or null if no mapping exists
     */
    public static String getString(final Context context, final String key) {
        try {
            if (null != context) {
                final SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
                return preferences.getString(key, null);
            }
        } catch (NullPointerException ne) {
            throw ne;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Get Int value for a particular key
     *
     * @param context
     * @param key
     * @return String value that was stored earlier, or null if no mapping exists
     */
    public static int getInt(final Context context, final String key) {

        try {
            if (null != context) {
                final SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
                return preferences.getInt(key, 0);
            }
        } catch (NullPointerException ne) {
            throw ne;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static double getDouble(final Context context, final String key) {


        try {
            if (null != context) {
                final SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
                double value = Double.longBitsToDouble(preferences.getLong(key, 0));
                return value;
            }
        } catch (NullPointerException ne) {
            throw ne;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Get boolean value for a particular key
     *
     * @param context
     * @param key
     * @return value or false if no mapping exists
     */
    public static boolean getBoolean(final Context context, final String key) {

        try {
            if (null != context) {
                final SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
                return preferences.getBoolean(key, false);
            }
        } catch (NullPointerException ne) {
            throw ne;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;


    }

    /**
     * Get boolean value for a particular key
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return value or default value specified if no mapping exists
     */
    public static boolean getBoolean(final Context context, final String key, boolean defaultValue) {

        try {
            if (null != context) {
                final SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
                return preferences.getBoolean(key, defaultValue);
            }
        } catch (NullPointerException ne) {
            throw ne;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Clears all sharedprefrence data for the specified content key.
     *
     * @param context activity context
     * @param name    file name / key name
     */
    public static void clearPreference(final Context context, final String name) {
        SharedPreferences preference = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.clear();
        editor.apply();
    }

    public static void logout(final Context context) {

        try{


        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

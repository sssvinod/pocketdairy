package com.tiara.pocketdairy.utils;

/**
 * Created by vinod on 11/23/17.
 */

public class ApplicationConstants {

    public static final int LOGIN_FB = 1;
    public static final int LOGIN_GOOGLE = 2;

    public static final String EMAIL_ID = "email_id";
    public static final String SOCIAL_ID = "social_id";
    public static final String REG_AS = "reg_as";
    public static final String NAME = "name";
    public static final String LOGIN_TYPE= "type";
    public static final String REG_AS_FB= "2";
    public static final String REG_AS_GOOGLE= "3";


    public static final int REQ_CODE_SOCIAL_LOGIN= 111;




    public class NetWorkConstants {

    }
}

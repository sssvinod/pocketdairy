package com.tiara.pocketdairy.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.tiara.pocketdairy.R;
import com.tiara.pocketdairy.adapter.NotepadAdapter;
import com.tiara.pocketdairy.fragments.FragmentCreateText;
import com.tiara.pocketdairy.fragments.FragmentHistoryText;

/**
 * Created by vinod on 12/20/17.
 */

public class NotepadActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);


        initValues();
    }

    private void initValues() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        NotepadAdapter adapter = new NotepadAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentCreateText(), "Your Text");
        adapter.addFragment(new FragmentHistoryText(), "History");
        viewPager.setAdapter(adapter);
    }

}
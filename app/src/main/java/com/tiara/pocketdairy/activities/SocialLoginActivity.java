package com.tiara.pocketdairy.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tiara.pocketdairy.model.FbResponceEntity;
import com.tiara.pocketdairy.model.GpResponceEntity;
import com.tiara.pocketdairy.utils.ApplicationConstants;

import org.json.JSONObject;

import java.util.Arrays;

import static com.tiara.pocketdairy.utils.ApplicationConstants.EMAIL_ID;
import static com.tiara.pocketdairy.utils.ApplicationConstants.NAME;
import static com.tiara.pocketdairy.utils.ApplicationConstants.REG_AS;
import static com.tiara.pocketdairy.utils.ApplicationConstants.REG_AS_FB;
import static com.tiara.pocketdairy.utils.ApplicationConstants.REG_AS_GOOGLE;
import static com.tiara.pocketdairy.utils.SharedPreferenceConstants.SOCIAL_ID;

public class SocialLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "SocialLoginActivity";
    private static final int RC_SIGN_IN = 9001;
    private Context mContext;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;

    int loginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        callbackManager = CallbackManager.Factory.create();
        super.onCreate(savedInstanceState);
        mContext = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        loginType = getIntent().getIntExtra(ApplicationConstants.LOGIN_TYPE, ApplicationConstants.LOGIN_FB);
        login(loginType);
    }

    private void login(int loginType) {

        switch (loginType) {
            case ApplicationConstants.LOGIN_FB:
                fbLogin();
                break;
            case ApplicationConstants.LOGIN_GOOGLE:
                googlePlussignIn();
                break;

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(loginType == ApplicationConstants.LOGIN_GOOGLE){
            mGoogleApiClient.connect();
        }

    }

    private void fbLogin() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Log.v(TAG, "login result : " + loginResult);
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code

                                        Log.d(TAG, "Response: " + object);

                                        Gson gson = new GsonBuilder().create();
                                        FbResponceEntity entity = new FbResponceEntity();

                                        if (object != null) {

                                            entity = gson.fromJson(object.toString(), FbResponceEntity.class);

                                            //TODO: remove later
                                            if (entity != null) {
                                                //TODO: next flow
                                                Intent returnIntent = new Intent();
                                                returnIntent.putExtra(EMAIL_ID, entity.getEmail());
                                                returnIntent.putExtra(SOCIAL_ID, entity.getId());
                                                returnIntent.putExtra(NAME, entity.getName());
                                                returnIntent.putExtra(REG_AS, REG_AS_FB);
                                                setResult(Activity.RESULT_OK, returnIntent);
                                                finish();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,link,gender,birthday,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                        finish();
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        finish();
                        // App code
                    }
                });

    }

    private void googlePlussignIn() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        try{
            if(null!=result){
                if (result.isSuccess()) { // Signin
                    Log.d(TAG, "handleSignInResult:" + result.isSuccess());
                    GoogleSignInAccount acct = result.getSignInAccount();
                    GpResponceEntity entity = new GpResponceEntity();

                    String personName = acct.getDisplayName();
                    String personEmail = acct.getEmail();
                    String personId = acct.getId();

                    Log.d("Name: ",personName );

                    entity.setEmail(personEmail);
                    entity.setName(personName);
                    entity.setId(personId);

                    if (entity != null) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(EMAIL_ID, entity.getEmail());
                        returnIntent.putExtra(SOCIAL_ID, entity.getId());
                        returnIntent.putExtra(NAME, entity.getName());
                        returnIntent.putExtra(REG_AS, REG_AS_GOOGLE);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                } else { // signout
                    Log.d("Google Plus Status: ",result.getStatus()+"");
                    finish();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        finish();
    }
}

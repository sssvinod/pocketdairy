package com.tiara.pocketdairy.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.tiara.pocketdairy.R;
import com.tiara.pocketdairy.model.UserPostRequest;
import com.tiara.pocketdairy.rest.ApiClient;
import com.tiara.pocketdairy.rest.ApiInterface;
import com.tiara.pocketdairy.utils.Utils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tiara.pocketdairy.utils.ApplicationConstants.EMAIL_ID;
import static com.tiara.pocketdairy.utils.ApplicationConstants.LOGIN_FB;
import static com.tiara.pocketdairy.utils.ApplicationConstants.LOGIN_GOOGLE;
import static com.tiara.pocketdairy.utils.ApplicationConstants.LOGIN_TYPE;
import static com.tiara.pocketdairy.utils.ApplicationConstants.NAME;
import static com.tiara.pocketdairy.utils.ApplicationConstants.REG_AS;
import static com.tiara.pocketdairy.utils.ApplicationConstants.REQ_CODE_SOCIAL_LOGIN;
import static com.tiara.pocketdairy.utils.SharedPreferenceConstants.SOCIAL_ID;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getName();
    private TextView mTvLoginFbBtn;
    private TextView mTvLoginGoogleBtn;
    private Utils mUtils;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        setupValuse();
    }

    private void init() {

        mTvLoginFbBtn = (TextView) findViewById(R.id.tvLoginFbBtn);
        mTvLoginGoogleBtn = (TextView) findViewById(R.id.tvLoginGoogleBtn);
    }

    private void setupValuse() {

        mContext = LoginActivity.this;
        mTvLoginFbBtn.setOnClickListener(this);
        mTvLoginGoogleBtn.setOnClickListener(this);

        mUtils = new Utils();
    }


    /**
     * validate all fields
     */

    private void validateFeeds() {

        if (mUtils.isNetworkAvailable(this)) {
            UserPostRequest userPostRequest = new UserPostRequest();
            userPostRequest.setType("");
            userPostRequest.setEmail("");
            doNetworkCallForLogin(userPostRequest);
        } else {
//            showResponseMessage(getResources().getString(R.string.alert_no_internet));
        }
    }

    private void showResponseMessage(String message) {
//        Snackbar snackbar = Snackbar
//                .make(mCrParentLayout, message, Snackbar.LENGTH_LONG);
//        snackbar.show();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tvLoginFbBtn) {
            Intent shareIntent = new Intent(this, SocialLoginActivity.class);
            shareIntent.putExtra(LOGIN_TYPE, LOGIN_FB);
            startActivityForResult(shareIntent, REQ_CODE_SOCIAL_LOGIN);
        } else if (v.getId() == R.id.tvLoginGoogleBtn) {
            Intent shareIntent = new Intent(this, SocialLoginActivity.class);
            shareIntent.putExtra(LOGIN_TYPE, LOGIN_GOOGLE);
            startActivityForResult(shareIntent, REQ_CODE_SOCIAL_LOGIN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_SOCIAL_LOGIN && resultCode == RESULT_OK) {
            try{
                String id = data.getStringExtra(SOCIAL_ID);
                String email = data.getStringExtra(EMAIL_ID);
                String name = data.getStringExtra(NAME);
                String regas = data.getStringExtra(REG_AS);

                UserPostRequest userPostRequest = new UserPostRequest();
                userPostRequest.setType(regas);
                userPostRequest.setFirst_name(name);
                userPostRequest.setEmail(email);
                userPostRequest.setSocial_id(id);
                doNetworkCallForLogin(userPostRequest);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public void doNetworkCallForLogin(UserPostRequest userPostRequest) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<JSONObject> loginCall = apiService.registerUser(userPostRequest);

        loginCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                int statusCode = response.code();
                Log.d(TAG, response + "");
                navigateToHomeActivity();
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void navigateToHomeActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
    }
}

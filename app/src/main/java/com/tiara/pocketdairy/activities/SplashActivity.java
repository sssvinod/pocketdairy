package com.tiara.pocketdairy.activities;

/**
 * Created by vinod on 9/22/17.
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.tiara.pocketdairy.R;
import com.tiara.pocketdairy.utils.SharedPreferenceConstants;
import com.tiara.pocketdairy.utils.SharedPreferencesManager;

public class SplashActivity extends Activity {
    private static final String TAG = SplashActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initValues();
        setUpValues();
    }

    private void initValues() {

    }


    private void setUpValues() {
        setStatusBarTranslucent(true);
        setTimer3Seconds();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void setTimer3Seconds() {

        int interval1 = 3000; // 10 Seconds
        Handler handler1 = new Handler();
        Runnable runnable1 = new Runnable() {
            public void run() {
                if (SharedPreferencesManager.getBoolean(SplashActivity.this, SharedPreferenceConstants.IS_LOGGED_IN)) {
                    navigateToLandingPage();
                } else {
                    navigateToLoginPage();
                }
            }
        };
        handler1.postAtTime(runnable1, System.currentTimeMillis() + interval1);
        handler1.postDelayed(runnable1, interval1);
    }

    private void navigateToLandingPage() {
        Intent homeActivityIntent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(homeActivityIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

    private void navigateToLoginPage() {
        Intent homeActivityIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(homeActivityIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

}
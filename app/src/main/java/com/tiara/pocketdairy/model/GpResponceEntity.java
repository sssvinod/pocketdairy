package com.tiara.pocketdairy.model;

import java.io.Serializable;

/**
 * Created by vinay on 3/9/16.
 */
public class GpResponceEntity implements Serializable {

    private String name;
    private String id;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
